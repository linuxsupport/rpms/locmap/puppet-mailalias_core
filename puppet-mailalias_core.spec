Name:		puppet-mailalias_core
Version:	1.0
Release:	3%{?dist}
Summary:	Masterless puppet module for mailalias_core

Group:		CERN/Utilities
License:	BSD
URL:		http://linux.cern.ch
Source0:	%{name}-%{version}.tgz

BuildArch:      noarch
Requires:       puppet-agent

%description
Masterless puppet module for mailalias_core.

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/mailalias_core/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/mailalias_core/
touch %{buildroot}/%{_datadir}/puppet/modules/mailalias_core/supporting_module


%files -n puppet-mailalias_core
%{_datadir}/puppet/modules/mailalias_core
%doc code/README.md

%changelog
* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 1.0-3
- Bump release for disttag change

* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 1.0-2
- add requires on puppet-agent

* Fri Feb 19 2021 Ben Morrice <ben.morrice@cern.ch> - 1.0-1
- initial release, required for puppet6+
